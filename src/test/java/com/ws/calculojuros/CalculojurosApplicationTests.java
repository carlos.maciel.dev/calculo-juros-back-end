package com.ws.calculojuros;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CalculojurosApplicationTests {

	@Test
	void contextLoads() {
		calcularParcelas(15000, 10);
	}

	public double calcularParcelas(double valorComJuros, int parcelas){

		double  resultadoParcelas = 0;

		resultadoParcelas = (valorComJuros / parcelas) ;

		return resultadoParcelas;


	}


}

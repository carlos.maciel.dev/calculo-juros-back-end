package com.ws.calculojuros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculojurosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculojurosApplication.class, args);
	}

}

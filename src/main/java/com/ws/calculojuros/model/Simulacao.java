package com.ws.calculojuros.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Simulacao")
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "simid")
    private long id;

    @Column(name="simvalorsolicitado", nullable = false)
    private Double valorSolicitado;

    @Column(name="simvalorjuros", nullable = true)
    private Double valorJuros;

    @Column(name="simvalortotaljuros", nullable = true)
    private Double valorTotalJuros;

    @Column(name = "simvalorparcela", nullable = true)
    private double valorParcela;

    @Column(name = "simqtdparcelas", nullable = false)
    private int quantidadeParcelas;

    @Column(name = "date", nullable = true)
    private Date dataInicio;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="cliid", nullable=false)
    private Cliente cliente;

    public Simulacao() {

    }

    public Simulacao(long id, Double valorSolicitado, Double valorJuros, Double valorTotalJuros, double valorParcela, int quantidadeParcelas, Date dataInicio, Cliente cliente) {
        this.id = id;
        this.valorSolicitado = valorSolicitado;
        this.valorJuros = valorJuros;
        this.valorTotalJuros = valorTotalJuros;
        this.valorParcela = valorParcela;
        this.quantidadeParcelas = quantidadeParcelas;
        this.dataInicio = dataInicio;
        this.cliente = cliente;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getValorSolicitado() {
        return valorSolicitado;
    }

    public void setValorSolicitado(Double valorSolicitado) {
        this.valorSolicitado = valorSolicitado;
    }

    public Double getValorJuros() {
        return valorJuros;
    }

    public void setValorJuros(Double valorJuros) {
        this.valorJuros = valorJuros;
    }

    public Double getValorTotalJuros() {
        return valorTotalJuros;
    }

    public void setValorTotalJuros(Double valorTotalJuros) {
        this.valorTotalJuros = valorTotalJuros;
    }

    public double getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(double valorParcela) {
        this.valorParcela = valorParcela;
    }

    public int getQuantidadeParcelas() {
        return quantidadeParcelas;
    }

    public void setQuantidadeParcelas(int quantidadeParcelas) {
        this.quantidadeParcelas = quantidadeParcelas;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public String toString() {
        return "Simulacao{" +
                "id=" + id +
                ", valorSolicitado=" + valorSolicitado +
                ", valorJuros=" + valorJuros +
                ", valorTotalJuros=" + valorTotalJuros +
                ", valorParcela=" + valorParcela +
                ", quantidadeParcelas=" + quantidadeParcelas +
                ", dataInicio=" + dataInicio +
                ", cliente=" + cliente +
                '}';
    }
}

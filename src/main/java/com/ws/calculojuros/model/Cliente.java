package com.ws.calculojuros.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="Cliente")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cliid")
    private long id;

    @Column(name = "clinome", nullable = false)
    private String nome;

    @Column(name = "clirenda", nullable = false)
    private Double renda;

    @Column(name = "clirisco", nullable = true)
    private int risco;

    @Column(name = "clirua", nullable = false)
    private String rua;

    @Column(name = "clinumero", nullable = true)
    private String numero;

    @Column(name = "clibairro", nullable = false)
    private String bairro;

    @Column(name = "clicep", nullable = false)
    private String cep;




    public Cliente() {

    }

    public Cliente(long id, String nome, Double renda, int risco, String rua, String numero, String bairro, String cep) {
        this.id = id;
        this.nome = nome;
        this.renda = renda;
        this.risco = risco;
        this.rua = rua;
        this.numero = numero;
        this.bairro = bairro;
        this.cep = cep;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getRenda() {
        return renda;
    }

    public void setRenda(Double renda) {
        this.renda = renda;
    }

    public int getRisco() {
        return risco;
    }

    public void setRisco(int risco) {
        this.risco = risco;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", renda=" + renda +
                ", risco=" + risco +
                ", rua='" + rua + '\'' +
                ", numero='" + numero + '\'' +
                ", bairro='" + bairro + '\'' +
                ", cep='" + cep + '\'' +
                '}';
    }
}





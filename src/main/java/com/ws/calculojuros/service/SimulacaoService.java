package com.ws.calculojuros.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ws.calculojuros.model.Cliente;
import com.ws.calculojuros.model.Simulacao;
import com.ws.calculojuros.repository.ClienteRepository;
import com.ws.calculojuros.repository.SimulacaoRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@Api(value = "Simulacao")
@RequestMapping(value = "/simulacao")
public class SimulacaoService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @CrossOrigin(origins = "*")
    @GetMapping("/{clienteId}/simulacoes")
    @ApiOperation(value = "Listar Simulacoes pelo Id do Cliente")
    public List<Simulacao> Get(@PathVariable(value = "clienteId") long id) {
        return simulacaoRepository.findByClienteId(id);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/{id}")
    @ApiOperation(value = "Listar simulação pelo Id da simulacao")
    public ResponseEntity<Simulacao> GetById(@PathVariable(value = "simulacaoId") long id)
    {
        Optional<Simulacao> simulacao = simulacaoRepository.findById(id);
        if(simulacao.isPresent())
            return new ResponseEntity<Simulacao>(simulacao.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/{clienteId}")
    @ApiOperation(value = "Criar nova Simulacao")
    public Optional<@Valid Simulacao> Post(@PathVariable(value = "clienteId") Long id, @Valid @RequestBody Simulacao simulacao) {

       return clienteRepository.findById(id).map(cliente -> {
        if(cliente.getRisco() == 1){
           simulacao.setValorJuros(calcularJuros(simulacao.getValorSolicitado(), 1.9 ));
           simulacao.setValorTotalJuros(calcularValorTotal(simulacao.getValorJuros(), simulacao.getValorSolicitado()));
           simulacao.setValorParcela(calcularParcelas(simulacao.getValorTotalJuros(), simulacao.getQuantidadeParcelas()));
        }
        if(cliente.getRisco() == 2){
           simulacao.setValorJuros(calcularJuros(simulacao.getValorSolicitado(), 5.0 ));
           simulacao.setValorTotalJuros(calcularValorTotal(simulacao.getValorJuros(), simulacao.getValorSolicitado()));
           simulacao.setValorParcela(calcularParcelas(simulacao.getValorTotalJuros(), simulacao.getQuantidadeParcelas()));

        }
        if(cliente.getRisco() == 3){
           simulacao.setValorJuros(calcularJuros(simulacao.getValorSolicitado(), 10.0 ));
           simulacao.setValorTotalJuros(calcularValorTotal(simulacao.getValorJuros(), simulacao.getValorSolicitado()));
           simulacao.setValorParcela(calcularParcelas(simulacao.getValorTotalJuros(), simulacao.getQuantidadeParcelas()));

        }
        Date date = new Date();
        simulacao.setDataInicio(date);

        simulacao.setCliente(cliente);
        return simulacaoRepository.save(simulacao);
        });


    }


//    @CrossOrigin(origins = "*")
//    @PutMapping("/{id}")
//    @ApiOperation(value = "Ataulizar Simulacao")
//    public ResponseEntity<Simulacao> Put(@PathVariable(value = "id") long id, @Valid @RequestBody Simulacao newSimulacao)
//    {
//        Optional<Simulacao> oldSimulacao = simulacaoRepository.findById(id);
//        if(oldSimulacao.isPresent()){
//            Simulacao simulacao = oldSimulacao.get();
//
//            simulacaoRepository.save(simulacao);
//            return new ResponseEntity<Simulacao>(simulacao, HttpStatus.OK);
//        }
//        else
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }

//    @CrossOrigin(origins = "*")
//    @PostMapping("/{id}")
//    @ApiOperation(value = "Calcular Juros")
//    public ResponseEntity<Object> Post(@PathVariable(value = "id") long id, double valor, int parcelas)
//    {
//
//        Optional<Cliente> cliente = clienteRepository.findById(id);
//
//        if(cliente.isPresent()){
//            Cliente clienteEscolhido = cliente.get();
//              String risco = clienteEscolhido.getRisco();
//
//           if(risco == "A"){
//               double juros = 1.9;
//               calcularJuros(valor, juros);
//           }
//
//            Cliente resultado;
//            return new ResponseEntity<>(resultado, HttpStatus.OK);
//        }
//        else
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }

    @CrossOrigin(origins = "*")
    @DeleteMapping(value = "{id}")
    @ApiOperation(value = "Delete Simulacao")
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id)
    {
        Optional<Simulacao> simulacao = simulacaoRepository.findById(id);
        if(simulacao.isPresent()){
            simulacaoRepository.delete(simulacao.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public double calcularJuros(double valor, double juros){

        double  resultado = 0;

        resultado = (valor * juros)/100;

        return resultado;


    }

    public double calcularValorTotal(double valorJuros, double valorSemJuros){

        double  resultado = 0;

        resultado = valorJuros + valorSemJuros;

        return resultado;


    }

    public double calcularParcelas(double valorComJuros, int parcelas){

        double  resultadoParcelas = 0;

        resultadoParcelas = (valorComJuros / parcelas) ;

        return resultadoParcelas;


    }
}

package com.ws.calculojuros.service;

import com.ws.calculojuros.model.Cliente;
import com.ws.calculojuros.repository.ClienteRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@Api(value = "Cliente")
@RequestMapping(value = "/cliente")
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @CrossOrigin(origins = "*")
    @GetMapping
    @ApiOperation(value = "Listar todos os clientes")
    public List<Cliente> Get() {
        return clienteRepository.findAll();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/{id}")
    @ApiOperation(value = "List Clientes by ID")
    public ResponseEntity<Cliente> GetById(@PathVariable(value = "id") long id)
    {
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if(cliente.isPresent())
            return new ResponseEntity<Cliente>(cliente.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @CrossOrigin(origins = "*")
//    @RequestMapping(value = "/cliente", method =  RequestMethod.POST)
    @PostMapping
    @ApiOperation(value = "Criar novo Cliente")
    public Cliente Post(@Valid @RequestBody Cliente cliente)
    {

        double renda;
        renda = cliente.getRenda();
        if(renda > 8000){
            cliente.setRisco(1);
        }
        if(renda > 2000 && renda <= 8000){
            cliente.setRisco(2);
         }
        if(renda <= 2000) {
            cliente.setRisco(3);
        }



        return clienteRepository.save(cliente);
    }


    @CrossOrigin(origins = "*")
    @PutMapping("/{id}")
    @ApiOperation(value = "Ataulizar Cliente")
    public ResponseEntity<Cliente> Put(@PathVariable(value = "id") long id, @Valid @RequestBody Cliente newCliente)
    {
        Optional<Cliente> oldCliente = clienteRepository.findById(id);
        if(oldCliente.isPresent()){
            Cliente cliente = oldCliente.get();
            cliente.setNome(newCliente.getNome());
            cliente.setRenda(newCliente.getRenda());
            cliente.setRisco(newCliente.getRisco());
            cliente.setRua(newCliente.getRua());
            cliente.setNumero(newCliente.getNumero());
            cliente.setBairro(newCliente.getBairro());
            cliente.setCep(newCliente.getCep());
            clienteRepository.save(cliente);
            return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

//    @CrossOrigin(origins = "*")
//    @PostMapping("/{id}")
//    @ApiOperation(value = "Calcular Juros")
//    public ResponseEntity<Object> Post(@PathVariable(value = "id") long id, double valor, int parcelas)
//    {
//
//        Optional<Cliente> cliente = clienteRepository.findById(id);
//
//        if(cliente.isPresent()){
//            Cliente clienteEscolhido = cliente.get();
//              String risco = clienteEscolhido.getRisco();
//
//           if(risco == "A"){
//               double juros = 1.9;
//               calcularJuros(valor, juros);
//           }
//
//            Cliente resultado;
//            return new ResponseEntity<>(resultado, HttpStatus.OK);
//        }
//        else
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }

    @CrossOrigin(origins = "*")
    @DeleteMapping(value = "{id}")
    @ApiOperation(value = "Delete Cliente")
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id)
    {
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if(cliente.isPresent()){
            clienteRepository.delete(cliente.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }




}
